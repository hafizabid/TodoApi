


using TodoApi.Messenger.Services;


namespace TodoApi
{
    public static class Startup
    {
        public static WebApplication InitializeApp(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            ConfigureServices(builder);
            var app = builder.Build();
            Configure(app);
            return app;

        }
        private static void ConfigureServices(WebApplicationBuilder builder)
        {
            // Adding CORS to allow Frontend-Access running on localhost.
            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddSingleton<IMessageService, MessageService>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        private static void Configure(WebApplication app)
        {


            // Configure the HTTP request pipeline.
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                app.UseSwagger();
                app.UseSwaggerUI();

            }
            app.UseSwagger();
            app.UseSwaggerUI();
            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();
        }


    }
}
