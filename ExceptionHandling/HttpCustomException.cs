using System;

namespace TodoApi.ExceptionHandling
{
    [Serializable]
    public class HttpCustomException : Exception
    {
        public int StatusCode { get; set; } 
        public string CustomMessage { get; set; }

        public HttpCustomException(int statusCode, string customMessage)
        {
            this.StatusCode = statusCode;
            this.CustomMessage = customMessage;
        }

        public HttpCustomException(int statusCode, string customMessage, Exception inner) : base(customMessage, inner)
        {
            this.StatusCode = statusCode;
            this.CustomMessage = customMessage;
        }
    }
}
