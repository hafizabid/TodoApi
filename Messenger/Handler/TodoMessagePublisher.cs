using System;
using System.Text;
using RabbitMQ.Client;

namespace TodoApi.Messenger.Services
{
    // define interface and service
    public interface IMessageService
    {
        bool Enqueue(string message);
    }

    public class MessageService : IMessageService
    {
        ConnectionFactory _factory;
        IConnection _conn;
        IModel _channel;
        public MessageService()
        {
            
            _factory = new ConnectionFactory() {HostName = Environment.GetEnvironmentVariable("RABBITMQ_HOST"), Port = Int32.Parse(Environment.GetEnvironmentVariable("RABBITMQ_PORT")) };
            _factory.UserName = Environment.GetEnvironmentVariable("RABBITMQ_USER");
            _factory.Password = Environment.GetEnvironmentVariable("RABBITMQ_PASSWORD");
            _conn = _factory.CreateConnection();
            _channel = _conn.CreateModel();
            _channel.QueueDeclare(queue: "books",
                                    durable: false,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);
        }
        public bool Enqueue(string messageString)
        {
            var body = Encoding.UTF8.GetBytes(messageString);
            _channel.BasicPublish(exchange: "",
                                routingKey: "books",
                                basicProperties: null,
                                body: body);
            return true;
        }
    }
}
