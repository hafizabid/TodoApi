using Microsoft.AspNetCore.Mvc;
using TodoApi.Messenger.Services;
using TodoApi.DBModel;
using System.Text.Json;
namespace TodoApi.Controllers;

[ApiController]
[Route("[controller]")]
public class BookPublisherController : BaseController
{
    private readonly IMessageService _messageService;
    private readonly ILogger<BookPublisherController> _logger;

    public BookPublisherController(ILogger<BookPublisherController> logger, IMessageService messageService)
    {
        _logger = logger;
        _messageService = messageService;
    }

    [HttpPost(Name = "PostBookPublisher")]
    public IActionResult Post(Book book)
    {
        try
        {
            _messageService.Enqueue(JsonSerializer.Serialize<Book>(book));
            return ManageResponse(StatusCodes.Status200OK, new { messasge = "The book has been added to the library."});
        }
        catch (Exception exception)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, exception.ToString());
        }


    }
}
